
const BASE_URL = "https://635f4b17ca0fe3c21a991d30.mockapi.io"
var idEdited;

getTaskList()
function getTaskList() {
    turnOnLoading()
    axios({
        method: "GET",
        url: `${BASE_URL}/todos`
    }).then(
        function (res) {
            renderTaskList(res.data)
            turnOffLoading()
        }

    ).catch(
        function (err) {
            turnOffLoading()
            console.log("err: ", err);
        }
    )
}



function addTask() {
    turnOnLoading()
    task = getFormInfo()
    axios({
        method: "POST",
        url: `${BASE_URL}/todos`,
        data: task,
    }).then(
        function (res) {
            turnOffLoading()
            getTaskList()
            console.log("res: ", res);
        }
    ).catch(
        function (err) {
            turnOffLoading()
            console.log("err: ", err);
        }
    )
}


function deleteTask(id) {
    turnOnLoading()
    axios({
        method: "DELETE",
        url: `${BASE_URL}/todos/${id}`
    }).then(
        function (res) {
            getTaskList()
            turnOffLoading()
            console.log(res.data);
        }
    ).catch(function (err) {
        turnOffLoading()
        console.log("Err: ", err);
    })
}

function updateTask() {
    updatedTask = getFormInfo()
    turnOnLoading()
    axios({
        method: "PUT",
        url: `${BASE_URL}/todos/${idEdited}`,
        data: updatedTask,
    }).then(function (res) {
        turnOffLoading()
        getTaskList()
    }).catch(
        function (err) {
            console.log("err: ", err);
        }
    )
}

function getTaskByID(id) {
    axios({
        method: "GET",
        url: `${BASE_URL}/todos/${id}`
    }).then(
        function (res) {
            showInfo(res.data)
            idEdited = res.data.id
        }
    ).catch(function (err) {
        console.log("err: ", err);
    })
}


function showInfo(task) {
    document.getElementById("list").value = task.list
    document.getElementById("title").value = task.title
    document.getElementById("note").value = task.note
    document.getElementById("status").value = task.status
    document.getElementById("startFrom").value = task.start_from
    document.getElementById("duration").value = task.duration
    document.getElementById("isCompleted").value = task.isCompleted
}


