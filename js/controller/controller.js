

function renderTaskList(taskList) {
    contentHTML = ""
    taskList.forEach(function (task) {
        contentHTML +=
            `
        <tr>
            <td>${task.id}</td>
            <td>${task.list}</td>
            <td>${task.title}</td>
            <td>${task.note}</td>
            <td>${task.status}</td>
            <td>${task.start_from}</td>
            <td>${task.duration}</td>
            <td>
                <input type="checkbox" ${task.isCompleted ? "checked" : ""}/>
            </td>
            <td>
                <button class="btn btn-success" onclick="getTaskByID(${task.id})"><i class="fa-sharp fa-solid fa-screwdriver-wrench"></i></button>
                <button class="btn btn-danger" onclick="deleteTask('${task.id}')"><i class="fa-solid fa-trash-can"></i></button>
            </td>
        </tr>
        `
    })
    document.getElementById('tbody').innerHTML = contentHTML
}


function getFormInfo() {
    let list = document.getElementById("list").value
    let title = document.getElementById("title").value
    let note = document.getElementById("note").value
    let status = document.getElementById("status").value
    let startFrom = document.getElementById("startFrom").value
    let duration = document.getElementById("duration").value

    let task = {
        list: list,
        title: title,
        note: note,
        status: status,
        start_from: startFrom,
        duration: duration
    };
    return task
}

function turnOnLoading() {
    document.getElementById('loading').style.display = "block"
}

function turnOffLoading() {
    document.getElementById('loading').style.display = "none"
}